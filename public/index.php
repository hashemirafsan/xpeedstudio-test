<?php declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../supports/helpers.php';

$bootstrap = new \XpeedStudio\Core\Bootstrap();
$bootstrap->load();
$bootstrap->dispatch();