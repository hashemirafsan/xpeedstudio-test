/**
 * found on Stackoverflow
 *
 * @param str
 * @returns {number}
 */
function word_count(str) {
     console.log(escape(str));
     str = str.replace(/(^\s*)|(\s*$)/gi,"");
     str = str.replace(/[ ]{2,}/gi," ");
     str = str.replace(/\n /,"\n");
     return str.split(' ').length;
}

/**
 *
 * @returns {string}
 */
function generate_class() {
     return btoa(Math.random()).substr(5, 5);
}

/**
 *
 * @param value
 * @param wrapperClass
 * @param fieldClass
 * @returns {string}
 */
function items_template(value, wrapperClass, fieldClass) {
     return `<div class="active_items row mb-2 ${wrapperClass}">
       <div class="col-md-11">
           <input type="text" name="items[]" class="form-control ${fieldClass}" value="${value}">
           <span class="danger"></span>
       </div>
       <div class="col-md-1">
           <button 
                class="btn btn-sm btn-danger delete_item" 
                type="button"
                data-wrapper=".${wrapperClass}"
                data-field=".${fieldClass}"
           >Delete</button>
       </div>
   </div>`;
}

/**
 *
 */
$(document).ready(function() {
     const $form = $('#xpeedstudio_form');
     const $submitButton = $('#submit');
     const $templateItemsWrapper = $('.template_items_wrapper');
     const $addItemsField = $('.add_items_field');
     const select2_buyer = $(".select2_buyer");
     const $dateRange = $('input[name="dates"]');
     const $phone = $("input[name='phone']");

     /**
      *
      * @type {{buyer_email: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, note: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, entry_by: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, amount: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, city: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, phone: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, receipt_id: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}, items: {messages: [string, string], selector: string, show_message_on_validate: boolean, validate: validate}, buyer: {messages: [string, string], selector: (jQuery|HTMLElement), show_message_on_validate: boolean, validate: (function(): boolean)}}}
      */
     const $fields = {
          amount: {
               selector: $("input[name='amount']"),
               validate: function() {
                    return /(?=^.{1,}$)(^[0-9]*$)/g.test(this.selector.val());
               },
               messages: [
                   "Amount field required",
                   "Supports only numbers"
               ],
               show_message_on_validate: false,
          },
          buyer: {
               selector: $("input[name='buyer']"),
               validate: function() {
                    return /(?=^.{1,20}$)(^[A-Za-z0-9_ ]*$)/g.test(this.selector.val());
               },
               messages: [
                   "Buyer field required",
                   "Supports text, space, numbers and max 20 letters"
               ],
               show_message_on_validate: false,
          },
          receipt_id: {
               selector: $("input[name='receipt_id']"),
               validate: function () {
                    return /(?=^.{1,}$)(^[A-Za-z]*$)/g.test(this.selector.val());
               },
               messages: [
                    "Receipt Id field required",
                    "Supports text only"
               ],
               show_message_on_validate: false,
          },
          buyer_email: {
               selector: $("input[name='buyer_email']"),
               validate: function () {
                    return /\S+@\S+\.\S+/g.test(this.selector.val());
               },
               messages: [
                   'Buyer email field required',
                   'Value must be email'
               ],
               show_message_on_validate: false,
          },
          items: {
               selector: 'input[name^="items"]',
               validate: function () {
                    let selector = $(this.selector, document);
                    if (! selector.length) {
                         return false;
                    }

                    let $status = true;
                    let that = this;
                    selector.each(function() {
                         if (! /(?=^.{1,}$)(^[A-Za-z]*$)/g.test($(this).val())) {
                              $(this).siblings('span.danger').text(that.messages.join('. '))
                         }
                         $status &= /(?=^.{1,}$)(^[A-Za-z]*$)/g.test($(this).val());
                    })
                    return $status;
               },
               messages: [
                    "Items field required",
                    "Supports text only"
               ],
               show_message_on_validate: true,
          },
          note: {
               selector: $("textarea[name='note']"),
               validate: function () {
                    console.log(word_count(this.selector.val()));
                    return !(word_count(this.selector.val()) > 30);
               },
               messages: [
                   "Supports text only",
                   "Maximum 30 words"
               ],
               show_message_on_validate: false,
          },
          city: {
               selector: $("input[name='city']"),
               validate: function () {
                    return /(?=^.{1,}$)(^[A-Za-z_ ]*$)/g.test(this.selector.val());
               },
               messages: [
                    "City field required",
                    "Supports text, space, numbers."
               ],
               show_message_on_validate: false,
          },
          phone: {
               selector: $phone,
               validate: function () {
                    return /(?=^.{1,13}$)(^[0-9]*$)/g.test(this.selector.val());
               },
               messages: [
                    "Phone field required",
                    "Supports numbers only and max 13 digit"
               ],
               show_message_on_validate: false,
          },
          entry_by: {
               selector: $("input[name='entry_by']"),
               validate: function () {
                    return /(?=^.{1,}$)(^[0-9]*$)/g.test(this.selector.val());
               },
               messages: [
                    "Phone field required",
                    "Supports numbers only"
               ],
               show_message_on_validate: false,
          }
     }

     /**
      *
      */
     $(document).on('click', '.add_item', function(e) {
          let $value = $addItemsField.val();
          let $genWrapperClass = generate_class();
          let $genFieldClass = generate_class();

          if ($value) {
               $templateItemsWrapper.before(items_template($value, $genWrapperClass, $genFieldClass));
          }
          $addItemsField.val('');
     });

     /**
      *
      */
     $(document).on('click', '.delete_item', function(e) {
          let $wrapperClass = $(this).data('wrapper');
          $($wrapperClass).remove();
     });

     /**
      *
      */
     $submitButton.click(function(e) {
          e.preventDefault();
          let validate = true;
          Object.keys($fields).forEach(function($key) {
               if (!$fields[$key].validate()) {
                    validate &= false;
                    if (!$fields[$key].show_message_on_validate) {
                         $($fields[$key].selector).siblings('span.danger').text($fields[$key].messages.join('. '));
                    }
               } else {
                    validate &= true;
                    $($fields[$key].selector).siblings('span.danger').text('');
               }
          });

          if (validate) {
               $.ajax({
                    url: $form.data('action'),
                    data: $form.serialize(),
                    type: "POST",
                    success: function (res) {
                         $form[0].reset();
                         alert(res.message);
                         $('.active_items').remove();
                    },
                    error: function(err) {
                         alert('Something went wrong');
                    }
               })
          }
     })

     $phone.keyup(function (e) {
          let val = $(this).val();
          if (! /^880/g.test(val)) {
               $(this).val('880' + $(this).val());
          }
     });

     $phone.focus(function (e) {
          let val = $(this).val();
          if (! /^880/g.test(val)) {
               $(this).val('880' + $(this).val());
          }
     });

     /**
      *
      */
     select2_buyer.select2({
          allowClear: true,
          minimumInputLength: 1,
          ajax: {
               url: select2_buyer.data('searchUrl') + '/',
               type: "GET",
               quietMillis: 50,
               data: function (data) {
                    return {
                         query: data.term,
                         entity: "buyer"
                    };
               },
               processResults: function (resp) {
                    return {
                         results: resp.data
                    }
               }
          }
     });

     /**
      *
      */
     select2_buyer.on('select2:select', function (e) {
          var searchParams = new URLSearchParams(window.location.search);
          if(searchParams.toString().length > 0 || $(this).val().length == 0)
          {
               if(searchParams.has($(this).data('key'))) {
                    searchParams.delete($(this).data('key'));
               }
          }

          if ($(this).val().length > 0) {
               searchParams.append($(this).data('key'), $(this).val());
          }
          var url= select2_buyer.data('redirectUrl') +'/?'+ searchParams.toString();
          window.location.replace(url);
     });

     /**
      *
      */
     $dateRange.daterangepicker({}, function(start, end, label) {
          let self = $dateRange;
          let value = start.format('YYYY-MM-DD') + ':' + end.format('YYYY-MM-DD');
          var searchParams = new URLSearchParams(window.location.search);
          if(searchParams.toString().length > 0 || value.length == 0)
          {
               if(searchParams.has(self.data('key'))) {
                    searchParams.delete(self.data('key'));
               }
          }

          if (value.length > 0) {
               searchParams.append(self.data('key'), value);
          }
          var url= self.data('redirectUrl') +'/?'+ searchParams.toString();
          window.location.replace(url);
     });
});

