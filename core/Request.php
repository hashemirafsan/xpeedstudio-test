<?php

namespace XpeedStudio\Core;

/**
 * Class Request
 * @package XpeedStudio\Core
 */
class Request
{
    /**
     * @var array
     */
    protected $query = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->setQuery(filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING) ?? []);

        if ($this->getMethod() === \XpeedStudio\Core\Enums\Request::HTTP_VERB_GET) {
            $this->setData(filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING) ?? []);
        }

        if ($this->getMethod() === \XpeedStudio\Core\Enums\Request::HTTP_VERB_POST) {
            $this->setData(filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING) ?? []);
        }
    }

    /**
     * @return array
     */
    public function all() : array
    {
        return $this->getData();
    }

    /**
     * @param string $fallback
     * @param null $default
     * @return array|mixed|null
     */
    public function get(string $fallback, $default = null)
    {
        return array_get($this->getData(), $fallback, $default);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function filled(string $key)
    {
        return isset($this->getData()[$key]);
    }

    /**
     * @param array $only
     * @return array
     */
    public function only(array $only) : array
    {
        $data = [];
        $remainKeys = array_intersect(array_keys($this->getData()), $only);
        foreach ($remainKeys as $key) {
            $data[$key] = $this->get($key);
        }
        return $data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $query
     */
    public function setQuery(array $query): void
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @return string
     */
    public function baseUri()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return  $this->baseUri() . rtrim("$_SERVER[REQUEST_URI]", '/');
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @param $path
     * @return bool
     */
    public function isSame($path)
    {
        return $_SERVER['REQUEST_URI'] === $path;
    }
}