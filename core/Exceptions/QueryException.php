<?php

namespace XpeedStudio\Core\Exceptions;

/**
 * Class QueryException
 * @package XpeedStudio\Core\Exceptions
 */
class QueryException extends \Exception
{
}