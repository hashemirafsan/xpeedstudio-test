<?php

namespace XpeedStudio\Core;

/**
 * Class Controller
 * @package XpeedStudio\Core
 */
abstract class Controller
{
    /**
     * Default method should be register
     * @return mixed
     */
    abstract public function index();

    /**
     * @param array $data
     * @param array $rules
     * @return Validator
     */
    public function validator(array $data, array $rules) : Validator
    {
        return Validator::make($data, $rules);
    }

    /**
     * @return Request
     */
    public function request() : Request
    {
        return (new Request());
    }
}