<?php


namespace XpeedStudio\Core;

/**
 * Class Cookie
 * @package XpeedStudio\Core
 */
class Cookie
{
    /**
     * @param $key
     * @param $value
     * @param null $ttl
     */
    public static function set($key, $value, $ttl = null) : void
    {
        setcookie($key, $value, time() + ($ttl ?? env('COOKIE_TTL')));
    }

    /**
     * @param $key
     * @return bool
     */
    public static function get($key) : bool
    {
        return isset($_COOKIE[str_replace('.', '_', $key)]);
    }
}