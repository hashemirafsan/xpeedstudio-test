<?php


namespace XpeedStudio\Core;

/**
 * Class Route
 * @package XpeedStudio\Core
 */
class Route
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Route constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param array|null $methods
     */
    public static function allowMethods(array $methods = null)
    {
        $check =  in_array(request()->getMethod(), $methods ?? [
            \XpeedStudio\Core\Enums\Request::HTTP_VERB_GET,
            \XpeedStudio\Core\Enums\Request::HTTP_VERB_POST,
        ]);

        if (! $check) {
            response()->json([
                'message' => "Method not allowed"
            ], \XpeedStudio\Core\Enums\Response::HTTP_METHOD_NOT_ALLOWED);
        }
    }

    /**
     * @return array
     */
    public function parseURL()
    {
        $explodedUrl = explode('/', $this->request->getPath());
        $explodedUrl = array_slice($explodedUrl, 3);
        [$controller, $method] = $explodedUrl;
        return [
            'controller' => $controller,
            'method'     => $method,
            'params'     => array_slice($explodedUrl, 2)
        ];
    }
}