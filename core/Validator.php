<?php

namespace XpeedStudio\Core;

/**
 * Class Validator
 * @package XpeedStudio\Core
 */
class Validator {

    /**
     * @var
     */
    protected $source;

    /**
     * @var
     */
    protected $rules;

    /**
     * @var array
     */
    private $parsedRules = [];

    /**
     * @var array[]
     */
    private $ruleSchema = [
        'required' => [
            'function' => 'required',
            'error' => [
                'message' => '%s field is required'
            ]
        ],
        'string' => [
            'function' => 'string',
            'error' => [
                'message' => '%s field not string',
            ]
        ],
        'numeric' => [
            'function' => 'numeric',
            'error' => [
                'message' => '%s field not numeric'
            ]
        ],
        'int' => [
            'function' => 'int',
            'error' => [
                'message' => '%s field not integer'
            ]
        ],
        'float' => [
            'function' => 'float',
            'error' => [
                'message' => '%s field not float'
            ]
        ],
        'boolean' => [
            'function' => 'boolean',
            'error' => [
                'message' => '%s field not boolean'
            ]
        ],
        'min' => [
            'function' => 'min',
            'error' => [
                'message' => '%s field value at least %s'
            ]
        ],
        'max' => [
            'function' => 'max',
            'error' => [
                'message' => '%s field value maximum %s'
            ]
        ],
        'array' => [
            'function' => 'array',
            'error' => [
                'message' => '%s field value not array'
            ]
        ],
        'match' => [
            'function' => 'match',
            'error' => [
                'message' => '%s not match with %s'
            ]
        ],
        'between' => [
            'function' => 'between',
            'error' => [
                'message' => '%s value is not between %s'
            ]
        ],
        'email' => [
            'function' => 'email',
            'error' => [
                'message' => '%s is not email'
            ]
        ],
        'words' => [
            'function' => 'words',
            'error' => [
                'message' => '%s field max length %s'
            ]
        ]

    ];

    /**
     * @var string[]
     */
    private $errorMessage = [
        'required' => '%s field is required',
        'string' => '%s field not string',
        'numeric' => '%s field not numeric',
        'int' => '%s field not integer',
        'float' => '%s field not float',
        'boolean' => '%s field not boolean',
        'min' => '%s field value at least %s',
        'max' => '%s field value maximum %s',
        'array' => '%s field value not array',
        'match' => '%s not match with %s',
        'between' => '%s value is not between %s',
        'email' => '%s is not email',
        'words' => '%s field max length %s',
    ];

    /**
     * @var array
     */
    private $errorBags = [];

    /**
     * @param array $errorBags
     */
    public function setErrorBags(array $errorBags): void
    {
        $this->errorBags = $errorBags;
    }

    /**
     * @param array $source
     * @param $rules
     * @return static
     */
    public static function make(array $source, $rules)
    {
        $static = new static();

        $static->setSource($source);
        $static->setRules($rules);

        $static->parseRules();
        
        return $static;
    }

    /**
     * @param $ruleName
     * @param callable $fn
     */
    private function setRuleSchema($ruleName, callable $fn)
    {
        
        $this->ruleSchema[$ruleName] = $fn;
    }

    /**
     * @param array $source
     * @return $this
     */
    protected function setSource(array $source) 
    {
        $this->source = $source;
        
        return $this;
    }

    /**
     * @param $rules
     * @return $this
     */
    protected function setRules($rules) 
    {
        $this->rules = $rules;
        
        return $this;
    }

    /**
     *
     */
    protected function parseRules()
    {
        if ($this->rules instanceof FormValidator) {
            $this->rules = $this->rules->rules();
        }

        foreach ($this->rules as $key => $rule) {
            
            

            if (is_string($rule) && strlen($rule) > 0) {

                $this->parsedRules[$key] = explode('|', $rule);
            }

            if (is_array($rule)) {
                $this->parsedRules[$key] = $rule;
            }

        }

    }

    /**
     * @return array
     */
    public function getParsedRules()
    {
        $this->parseRules();

        return $this->parsedRules;
    }

    /**
     * @param null $key
     * @return bool
     */
    public function validate($key = null) : bool
    {
        $parsedRules = ! is_null($key) ? $this->parsedRules[$key] : $this->parsedRules;

        $validate = true;
        
        foreach ($parsedRules as $key => $rules) {
            foreach ($rules as $rule) {
                $parse = $this->validateRules(explode(':', $rule));

                if (count($parse) > 1) {
                    [$methodName, $params] = $parse;

                    if (method_exists($this, sprintf("method%s", ucwords($methodName)))) {
                        $result = call_user_func_array([$this, sprintf("method%s", ucwords($methodName))], [$key, $params]);
                    }
                    if (!$result) {
                        $this->errorBags[$key][] = $this->setErrorMessage($key, $parse);
                    }

                    $validate &= $result;

                } else {

                    [$methodName] = $parse;

                    $result = call_user_func_array([$this, sprintf("method%s", ucwords($methodName))], [$key]);    
                    
                    if (!$result) {
                        $this->errorBags[$key][] = $this->setErrorMessage($key, $parse);
                    }

                    $validate &= $result;
                }
            }
        }

        return (bool) $validate;
    }

    /**
     * @param $rules
     * @return mixed
     */
    private function validateRules($rules)
    {
        [$ruleName] = $rules;
        if (!isset($this->ruleSchema[$ruleName])) {
            throw new \Error("$ruleName is not a rule!");
        }

        return $rules;
    }

    /**
     * @param $key
     * @param null $value
     * @return bool
     */
    private function methodUrl($key, $value = null)
    {
        if (! is_null($value)) {
            return $this->source[$key] === $value;
        }

        return (filter_var($this->source[$key], FILTER_VALIDATE_URL)) ? true : false;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    private function methodBetween($key, $value)
    {
        if (! $this->methodNumeric($key)) {
            return false;
        }
        
        [$to, $from] = explode(',', $value);

        return $this->source[$key] >= $to && $this->source[$key] <= $from;

    }

    /**
     * @param $source
     * @param $compare
     * @return bool
     */
    private function methodMatch($source, $compare)
    {
        if (! isset($this->source[$source]) && ! isset($this->source[$compare])) {
            return false;
        }

        return $this->source[$source] === $this->source[$compare];

    }

    /**
     * @param $key
     * @return bool
     */
    private function methodEmail($key)
    {
        if (! isset($this->source[$key])) {
            return false;
        }

        return (filter_var($this->source[$key], FILTER_VALIDATE_EMAIL)) ? true : false;
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodBoolean($key)
    {
        return is_bool($this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodInt($key)
    {
        if (! isset($this->source[$key])) {
            return false;
        }
        return is_int((int) $this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodFloat($key)
    {
        return is_float((float) $this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodRequired($key)
    {
        return isset($this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodNumeric($key)
    {
        if (! $this->methodRequired($key)) {
            return false;
        }
        return is_numeric($this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodArray($key)
    {
        if (! isset($this->source[$key])) {
            return false;
        }
        return is_array($this->source[$key]);
    }

    /**
     * @param $key
     * @return bool
     */
    private function methodString($key)
    {
        return is_string($this->source[$key]);
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    private function methodMin($key, $value)
    {
        if (! isset($this->source[$key])) {
            return false;
        }

        if ($this->methodString($key)) {
            return strlen($this->source[$key]) >= $value;
        }

        if ($this->methodNumeric($key)) {
            return $this->source[$key] >= $value;
        }
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    private function methodMax($key, $value)
    {

        if (! isset($this->source[$key])) {
            return false;
        }

        if ($this->methodString($key)) {
            return strlen($this->source[$key] ?? '') <= $value;
        }

        if ($this->methodNumeric($key)) {
            return $this->source[$key] <= $value;
        }
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function methodWords($key, $value)
    {
        if (! isset($this->source[$key])) {
            return false;
        }

        if ($this->methodString($key)) {
            return str_word_count(utf8_encode($this->source[$key]) ?? '') <= $value;
        }
    }

    /**
     * @param $field
     * @param $params
     * @return string
     */
    private function setErrorMessage($field, $params)
    {   
        $key = array_shift($params);
        
        return sprintf($this->errorMessage[$key], $field, ...$params);
    }

    /**
     * @return bool
     */
    public function fails()
    {
        $this->setErrorBags([]);

        return ! $this->validate();
    }

    /**
     * @return array
     */
    public function errors()
    {
        $this->setErrorBags([]);

        $this->validate();

        return $this->errorBags;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key) : bool
    {
        if (isset($this->errorBags[$key])) {
            return true;
        }

        return false;
    }

    /**
     * @param $ruleName
     * @param callable $fn
     */
    public function addRules($ruleName, callable $fn)
    {

    }
    
}
