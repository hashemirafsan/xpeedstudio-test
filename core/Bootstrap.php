<?php


namespace XpeedStudio\Core;

use XpeedStudio\Controllers\HomeController;
use XpeedStudio\Core\DB\Connection;

/**
 * Class Bootstrap
 * @package XpeedStudio\Core
 */
class Bootstrap
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Route
     */
    protected $route;

    /**
     * @var string
     */
    protected $namespace = "XpeedStudio\\Controllers";

    public function __construct()
    {
        $this->request = (new Request());
        $this->route = (new Route($this->request));
    }

    /**
     *
     */
    public function load() : void
    {
        $this->loadConfigIni();
        $this->install();
    }

    /**
     *
     */
    private function loadConfigIni(): void
    {
        //TODO: Need to implement caching
        $parseIni = parse_ini_file(base_dir('.env.ini'));
        foreach ($parseIni as $key => $value) {
            $_ENV[$key] = !empty($value) ? $value : null;
        }
    }

    /**
     * @return mixed
     */
    public function dispatch()
    {
        $data = $this->route->parseURL();
        if (!empty($data['controller'])) {
            $controller = $this->namespace . '\\' . ucfirst($data['controller']) . 'Controller';
            [$method] = explode('?', ($data['method'] ?? 'index'));
            return call_user_func_array([(new $controller()), $method], [$this->request]);
        } else {
            return call_user_func_array([(new HomeController()), 'index'], [$this->request]);
        }
    }

    /**
     * @return bool
     */
    private function install()
    {
        try {
            $connection = (new Connection())->getConnection();
            $status = $connection->getAttribute(\PDO::ATTR_CONNECTION_STATUS);

            $results = $connection->query("show tables like 'orders'");

            if ($results->rowCount() > 0) {
                return true;
            } else {
                $connection->exec("DROP TABLE IF EXISTS `orders`;
                    CREATE TABLE `orders` (
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `amount` int(10) NOT NULL,
                      `buyer` varchar(255) NOT NULL,
                      `receipt_id` varchar(20) NOT NULL,
                      `items` varchar(255) NOT NULL,
                      `buyer_email` varchar(50) NOT NULL,
                      `buyer_ip` varchar(20) NOT NULL,
                      `note` text DEFAULT NULL,
                      `city` varchar(20) NOT NULL,
                      `phone` varchar(20) NOT NULL,
                      `hash_key` varchar(255) NOT NULL,
                      `entry_at` date NOT NULL,
                      `entry_by` int(10) NOT NULL,
                      PRIMARY KEY (`id`)
                )");
            }
        } catch (\Exception $exception) {
            return false;
        }

        return true;

    }
}