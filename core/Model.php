<?php

namespace XpeedStudio\Core;

use XpeedStudio\Core\DB\Connection;
use XpeedStudio\Core\DB\Query;

/**
 * Class Model
 * @package XpeedStudio\Core
 */
abstract class Model extends Query implements \JsonSerializable, \ArrayAccess
{
    /**
     * @var string
     */
    protected $tableName = '';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [];

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $original = [];

    /**
     * @param mixed $tableName
     */
    public function setTableName($tableName): void
    {
        $this->tableName = $tableName;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @return string
     */
    protected function getModelKey() : string
    {
        return 'id';
    }

    /**
     * @return mixed
     */
    public function getDefaultSortKey()
    {
        return 'id|desc';
    }

    /**
     * @param array $guarded
     */
    public function setGuarded(array $guarded): void
    {
        $this->guarded = $guarded;
    }

    /**
     * @return array
     */
    public function getGuarded(): array
    {
        return $this->guarded;
    }

    /**
     * @param array $casts
     */
    public function setCasts(array $casts): void
    {
        $this->casts = $casts;
    }

    /**
     * @return array
     */
    public function getCasts(): array
    {
        return $this->casts;
    }

    /**
     * @param array $fillable
     */
    public function setFillable(array $fillable): void
    {
        $this->fillable = $fillable;
    }

    /**
     * @return array
     */
    public function getFillable(): array
    {
        return $this->fillable;
    }

    /**
     * @param array $original
     * @return $this|void
     */
    public function setOriginal(array $original): Model
    {
        $this->original = $original;
        return $this;
    }

    /**
     * @return array
     */
    public function getOriginal(): array
    {
        return $this->original;
    }

    public function __set($name, $value)
    {
        $this->original[$name] = $value;
    }

    /**
     * @param $property
     * @return array|mixed|null
     */
    public function __get($property)
    {
        $value = array_get($this->getOriginal(), $property, null);
        if (method_exists($this, sprintf('get%sProperty', camel_case($property)))) {
            $value = call_user_func_array([$this, sprintf('get%sProperty', camel_case($property))], [$value]);
        }
        return $value;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->getOriginal();
    }

    public function offsetExists($offset)
    {
        return isset($this->original[$offset]);
    }

    public function offsetGet($offset)
    {
        return array_get($this->getOriginal(), $offset, null);
    }

    public function offsetSet($offset, $value)
    {
        $this->original[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->original[$offset]);
    }
}