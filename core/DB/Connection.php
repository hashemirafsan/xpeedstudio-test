<?php

namespace XpeedStudio\Core\DB;

use PDO;

/**
 * Class Connection
 * @package XpeedStudio\Core\DB
 */
class Connection
{
    /**
     * @var
     */
    protected $host;

    /**
     * @var
     */
    protected $port;

    /**
     * @var
     */
    protected $dbname;

    /**
     * @var
     */
    protected $user;

    /**
     * @var
     */
    protected $password;

    /**
     * @var
     */
    protected $connection;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $this->setHost(env('DB_HOST', 'localhost'));
        $this->setPort(env('DB_PORT', '3306'));
        $this->setDbname(env('DB_DATABASE', 'xpeedstudio'));
        $this->setUser(env('DB_USER', 'guest'));
        $this->setPassword(env('DB_PASSWORD', 'forge'));

        if (empty($this->connection)) {
            try {
                $this->setConnection(
                    $this->getHost(),
                    $this->getPort(),
                    $this->getDbname(),
                    $this->getUser(),
                    $this->getPassword()
                );
            } catch (\PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
    }

    /**
     * @param $host
     * @param $port
     * @param $dbname
     * @param $user
     * @param $password
     */
    public function setConnection($host, $port, $dbname, $user, $password): void
    {
        $this->connection = new PDO("mysql:host={$host};port={$port};dbname={$dbname}", $user, $password);;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    /**
     * @param mixed|string $host
     */
    public function setHost($host): void
    {
        $this->host = $host;
    }

    /**
     * @return mixed|string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed|string $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed|string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed|string $port
     */
    public function setPort($port): void
    {
        $this->port = $port;
    }

    /**
     * @return mixed|string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed|string $dbname
     */
    public function setDbname($dbname): void
    {
        $this->dbname = $dbname;
    }

    /**
     * @return mixed|string
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param mixed|string $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed|string
     */
    public function getPassword()
    {
        return $this->password;
    }
}