<?php

namespace XpeedStudio\Core\DB;

use XpeedStudio\Core\Exceptions\QueryException;

/**
 * Class Query
 * @package XpeedStudio\Core\DB
 */
class Query
{
    /**
     * @var
     */
    protected $connection;

    /**
     * @var array
     */
    protected static $select = [];

    /**
     * @var array
     */
    protected static $where = [];

    /**
     * @var array
     */
    protected static $orderBy = [];

    /**
     * Query constructor.
     */
    public function __construct()
    {
        $this->setConnection((new Connection())->getConnection());
        $this->getConnection()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return static
     */
    public static function query(): Query
    {
        return new static;
    }

    /**
     * @param string $column
     * @param string $operator
     * @param $value
     * @param bool $or
     * @param bool $multiple
     * @return Query
     */
    public static function where(string $column, string $operator, $value, bool $or = false, bool $multiple = false): Query
    {
        static::query()->setWhere([
            'column' => $column,
            'operator' => $operator,
            'value' => $value,
            'or' => $or,
            'multiple_parameter' => $multiple
        ]);

        return static::query();
    }

    /**
     * @param string $column
     * @param string $operator
     * @param $value
     * @param bool $or
     * @param bool $multiple
     * @return Query
     */
    public static function orWhere(string $column, string $operator, $value, $or = true, $multiple = false): Query
    {
        return static::where($column, $operator, $value, $or, $multiple);
    }

    /**
     * @param string $column
     * @param array $range
     * @param bool $or
     * @return Query
     */
    public static function whereBetween(string $column, array $range, bool $or = false) : Query
    {
        [$from, $to] = $range;
        return static::where($column, 'between', [
            ':from' => $from,
            ':to'   => $to
        ], $or, true);
    }

    /**
     * @param array $where
     */
    public static function setWhere(array $where): void
    {
        static::$where[] = [
            'column'   => $where['column'],
            'operator' => $where['operator'],
            'value'    => $where['value'],
            'or'       => $where['or'],
            'multiple_parameter' => $where['multiple_parameter']
        ];
    }

    /**
     * @return array
     */
    public static function getWhere(): array
    {
        return self::$where;
    }

    /**
     * @param string[] $select
     */
    public static function setSelect(array $select): void
    {
        self::$select = array_merge(self::$select, $select);
    }

    /**
     * @return string[]
     */
    public static function getSelect(): array
    {
        return self::$select;
    }

    /**
     * @param array $orderBy
     */
    public static function setOrderBy(array $orderBy): void
    {
        self::$orderBy[] = $orderBy;
    }

    /**
     * @return array
     */
    public static function getOrderBy(): array
    {
        return self::$orderBy;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection): void
    {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    private function prepare() : string
    {
        $selectQuery = $this->_prepareSelectQuery();
        $whereQuery  = $this->_prepareWhereQuery();
        $sortQuery   = $this->_prepareSortByQuery();

        return sprintf("%s %s %s", $selectQuery, implode("", $whereQuery), $sortQuery);
    }

    /**
     * @return string
     */
    public static function getSql() : string
    {
        return static::query()->prepare();
    }

    /**
     * @return mixed
     */
    public function get()
    {
        try {
            $query = static::query()->getConnection()->prepare(
                static::query()->prepare()
            );

            $values = [];
            foreach (static::getWhere() as $item) {
                if (!$item['multiple_parameter']) {
                    $values[$item['column']] = $item['value'];
                } else {
                    foreach ($item['value'] as $column => $value) {
                        $values[$column] = $value;
                    }
                }
            }

            $query->execute($values);

            $results = [];

            foreach ($query->fetchAll(\PDO::FETCH_ASSOC) ?? [] as $item) {
                $results[] = static::query()->bindWithModel(static::query()->filterModelActions($item));
            }

            return $results;
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
            die();
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public static function create(array $params)
    {
        $connection = static::query()->getConnection();
        try {
            $query = $connection->prepare(
                static::query()->_prepareCreateQuery(call_user_func([static::query(), 'getFillable']))
            );

            $query->execute(static::query()->getFillableData($params));

            return $connection->lastInsertId();
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
            die();
        }
    }

    /**
     * @param array $columns
     * @return Query
     */
    public static function select(array $columns) : Query
    {
        static::setSelect($columns);
        return static::query();
    }

    /**
     * @param $param
     * @return mixed
     * @throws QueryException
     */
    public static function find($param)
    {
        try {
            static::query()->setWhere([
                'column' => call_user_func([static::query(), 'getModelKey']),
                'operator' => '=',
                'value' => $param,
                'or' => false
            ]);

            $query = static::query()->getConnection()->prepare(
                static::query()->prepare()
            );

            $query->execute([
                call_user_func([static::query(), 'getModelKey']) => $param
            ]);

            return static::query()->bindWithModel(static::query()->filterModelActions($query->fetch(\PDO::FETCH_ASSOC)));
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
            die();
        }
    }

    private function bindWithModel($result)
    {
        return call_user_func_array([static::query(), 'setOriginal'], [$result ?? []]);
    }

    /**
     * @param string $column
     * @param string $orderBy
     * @return Query
     */
    public static function orderBy(string $column, string $orderBy = 'asc') : Query
    {
        static::setOrderBy([
            'column' => $column,
            'order_by' => $orderBy
        ]);

        return static::query();
    }

    /**
     * @param array $keys
     * @return string
     */
    private function _prepareCreateQuery(array $keys) : string
    {
        $column = implode(', ', $keys);
        $bindValue = implode(', ', array_map(function($key) {
            return ":{$key}";
        }, $keys));

        return sprintf("insert into %s (%s) values (%s)", call_user_func([$this, 'getTableName']), $column, $bindValue);
    }

    /**
     * @return string
     */
    private function _prepareSelectQuery() : string
    {
        $select = count(static::getSelect()) ? static::getSelect() : ['*'];
        return sprintf("select %s from %s", implode(',', $select), call_user_func([$this, 'getTableName']));
    }

    /**
     * @return array
     */
    private function _prepareWhereQuery() : array
    {
        $whereStmt = [];
        foreach (static::getWhere() as $index => $item) {
            if ($index == 0) {
                if (!$item['multiple_parameter']) {
                    $whereStmt[] = "where {$item['column']} {$item['operator']} :{$item['column']}";
                } else {
                    $keys = implode(' and ', array_keys($item['value']));
                    $whereStmt[] = "where {$item['column']} {$item['operator']} {$keys}";
                }
            } else {
                if (!$item['multiple_parameter']) {
                    $whereStmt[] = ($item['or'] ? " or " : " and ") . "{$item['column']} {$item['operator']} :{$item['column']}";
                } else {
                    $keys = implode(' and ', array_keys($item['value']));
                    $whereStmt[] = ($item['or'] ? " or " : " and ") . "{$item['column']} {$item['operator']} {$keys}";
                }
            }
        }
        return $whereStmt;
    }

    /**
     * @return string
     */
    private function _prepareSortByQuery() : string
    {
        if (count(static::getOrderBy())) {
            $result = [];
            foreach (static::getOrderBy() as $orderBy) {
                $result[] = "{$orderBy['column']} {$orderBy['order_by']}";
            }
            return sprintf("order by %s", implode(', ', $result));
        } else {
             [$column, $sort]= explode('|', call_user_func([$this, 'getDefaultSortKey']));
            return sprintf("order by %s %s", $column, $sort);
        }
    }

    /**
     * @param $params
     * @return array
     */
    private function getFillableData($params) : array
    {
        $fillable = $this->getFillable();
        $paramsKey = array_keys($params);
        $remainParams = array_intersect($fillable, $paramsKey);
        $data = [];

        foreach ($remainParams as $key) {
            $data[$key] = $params[$key] ?? null;
        }

        return $data;
    }

    /**
     * @param $param
     * @return mixed
     */
    private function filterModelActions($param)
    {
        $param = $this->getCastsData($param);
        return $param;
    }

    /**
     * @param $param
     * @return mixed
     */
    private function getCastsData($param)
    {
        $casts = $this->getCasts();
        foreach($casts as $column => $action) {
            $param[$column] = call_user_func_array([$this, sprintf("cast%s", ucwords($action))], [$param[$column]]);
        }
        return $param;
    }

    /**
     * @param $value
     * @return mixed
     */
    private function castUnserialize($value)
    {
        return unserialize(base64_decode($value));
    }
}