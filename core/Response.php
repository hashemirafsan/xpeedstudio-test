<?php

namespace XpeedStudio\Core;

/**
 * Class Response
 * @package XpeedStudio\Core
 */
class Response
{
    /**
     * @param array $params
     * @param int $httpCode
     */
    public function json(array $params, $httpCode = \XpeedStudio\Core\Enums\Response::HTTP_OK)
    {
        header('Content-type:application/json;charset=utf-8');
        http_response_code($httpCode);
        echo json_encode($params, $httpCode);
        die();
    }
}