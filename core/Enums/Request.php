<?php

namespace XpeedStudio\Core\Enums;

/**
 * Interface Request
 * @package XpeedStudio\Core\Enums
 */
interface Request
{
    const HTTP_VERB_GET = "GET";
    const HTTP_VERB_POST = "POST";
}