<?php


namespace XpeedStudio\Core\Enums;

/**
 * Interface Response
 * @package XpeedStudio\Core\Enums
 */
interface Response
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_UNPROCESSABLE_ENTITY = 422;
    const HTTP_NOT_FOUND = 404;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_METHOD_NOT_ALLOWED = 405;
}