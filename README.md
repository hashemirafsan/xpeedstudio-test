# XpeedStudio MVC Test

![alt text](https://gitlab.com/hashemirafsan/xpeedstudio-test/-/raw/master/public/assets/screenshots/request_floq.png)  
When a user request to this application at `public/index.php`, application autoload all the files and helpers.
After that `core/Bootstrap.php` initiate and load all the config and check database connection and anything need to install or not.
When everything is loaded then dispatch the request.

## Requirements
1) PHP 7.2+
2) MySQL
3) Composer 
4) `ext-json`


## Install
1. copy `.env.ini.example` to `.env.ini`in application `root` directory
2. set database value to `.env.ini`
3. run `composer dump-autoload`
4. goto `public/index.php`
5. run your application `php -S localhost:8001`
6. open browser and go to `http://localhost:8001`

## Structure
- __xpeedstudio__
   - [README.md](README.md)
   - [composer.json](composer.json)
   - __core__
     - [Bootstrap.php](core/Bootstrap.php)
     - [Controller.php](core/Controller.php)
     - [Cookie.php](core/Cookie.php)
     - __DB__
       - [Connection.php](core/DB/Connection.php)
       - [Query.php](core/DB/Query.php)
     - __Enums__
       - [Request.php](core/Enums/Request.php)
       - [Response.php](core/Enums/Response.php)
     - __Exceptions__
       - [QueryException.php](core/Exceptions/QueryException.php)
     - [Model.php](core/Model.php)
     - [Request.php](core/Request.php)
     - [Response.php](core/Response.php)
     - [Route.php](core/Route.php)
     - [Validator.php](core/Validator.php)
   - __public__
     - __assets__
       - __css__
         - [bootstrap.min.css](public/assets/css/bootstrap.min.css)
         - [select2.min.css](public/assets/css/select2.min.css)
         - [style.css](public/assets/css/style.css)
       - __js__
         - [bootstrap.min.js](public/assets/js/bootstrap.min.js)
         - [functions.js](public/assets/js/functions.js)
         - [jquery.min.js](public/assets/js/jquery.min.js)
         - [popper.min.js](public/assets/js/popper.min.js)
         - [select2.full.min.js](public/assets/js/select2.full.min.js)
     - [index.php](public/index.php)
   - __sql__
     - [orders.sql](sql/orders.sql)
   - __src__
     - __Controllers__
       - [HomeController.php](src/Controllers/HomeController.php)
     - __Models__
       - [Order.php](src/Models/Order.php)
     - __Service__
       - [OrderService.php](src/Service/OrderService.php)
     - __resource__
       - __views__
         - [home.php](src/resource/views/home.php)
         - [list.php](src/resource/views/list.php)
         - __partials__
           - [footer.php](src/resource/views/partials/footer.php)
           - [header.php](src/resource/views/partials/header.php)
   - __supports__
     - [helpers.php](supports/helpers.php)



