<?php

namespace XpeedStudio\Models;

use XpeedStudio\Core\Model;

/**
 * Class Order
 * @package XpeedStudio\Models
 */
class Order extends Model
{
    /**
     * @var string
     */
    protected $tableName = 'orders';

    /**
     * @var string[]
     */
    protected $fillable = [
        'amount',
        'buyer',
        'receipt_id',
        'buyer_email',
        'city',
        'phone',
        'entry_by',
        'items',
        'hash_key',
        'buyer_ip',
        'entry_at',
        'note',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'items' => 'unserialize'
    ];
}