<?php

namespace XpeedStudio\Service;

use XpeedStudio\Models\Order;

/**
 * Class OrderService
 * @package XpeedStudio\Service
 */
class OrderService
{
    /**
     * @param array $params
     * @return mixed
     */
    public function store(array $params)
    {
        $params = array_merge($params, [
            'items'    =>  base64_encode(serialize($params['items'])),
            'hash_key' => $this->generateHashKey($params['receipt_id']),
            'buyer_ip' => request()->getIp(),
            'entry_at' => date('Y-m-d'),
        ]);

        $order = new Order();
        return $order->create($params);
    }

    /**
     * @param string $str
     * @return string|null
     */
    protected function generateHashKey(string $str)
    {
        return crypt($str, env('SALT_KEY'));
    }
}