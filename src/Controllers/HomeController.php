<?php

namespace XpeedStudio\Controllers;

use XpeedStudio\Core\{Cookie, Route, Request, Controller, Validator};
use XpeedStudio\Core\Enums\{
    Response,
    Request as RequestEnum
};
use XpeedStudio\Models\Order;
use XpeedStudio\Service\OrderService;

/**
 * Class HomeController
 * @package XpeedStudio\Controllers
 */
class HomeController extends Controller {

    const ORDER_CREATE_SUSPENDED = "Order Creation suspended!";
    const ORDER_CREATE_FAILED = "Something went wrong in order creation, Please Try Again";
    const ORDER_CREATED_SUCCESSFULLY = "Order Created Successfully!";

    /**
     * @var
     */
    protected $orderService;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->setOrderService(OrderService::class);
    }

    /**
     * @param mixed $orderService
     */
    public function setOrderService($orderService): void
    {
        $this->orderService = (new $orderService());
    }

    /**
     * @return mixed|void
     */
    public function index()
    {
        Route::allowMethods([RequestEnum::HTTP_VERB_GET]);

        return view('home');
    }

    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        Route::allowMethods([RequestEnum::HTTP_VERB_GET]);

        $query = Order::query();

        if ($request->filled('buyer')) {
            $query->where('buyer', 'LIKE', '%' . $request->get('buyer') . '%');
        }

        if ($request->filled('range')) {
            [$from, $to] = explode(':', $request->get('range'));
            $query->whereBetween('entry_at', [$from, $to]);
        }

        $results = $query->get();

        return view('list', compact('results'));
    }

    /**
     * @param Request $request
     */
    public function search(Request $request)
    {
        Route::allowMethods([RequestEnum::HTTP_VERB_GET]);

        $entity = $request->get('entity');

        if ($entity === "buyer") {
            $orders = Order::select(['buyer'])->where('buyer', 'LIKE', '%' . $request->get('query') . '%')->get();
            $results = [];
            foreach ($orders as $order) {
                $results[] = [
                    'id' => $order['buyer'],
                    'text' => $order['buyer']
                ];
            }

            return response()->json([
                'data' => $results,
            ]);
        }
    }

    /**
     * @param Request $request
     * @throws \XpeedStudio\Core\Exceptions\QueryException
     */
    public function store(Request $request)
    {
        Route::allowMethods([RequestEnum::HTTP_VERB_POST]);

        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'buyer'  => 'required|min:1|max:20',
            'receipt_id' => 'required',
            'items' => 'required|array',
            'buyer_email' => 'required|email',
            'city' => 'required|string|max:20',
            'phone' => 'required|numeric',
            'entry_by' => 'required|int',
            'note' => 'words:30',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (Cookie::get($request->get('buyer_email'))) {
            return response()->json([
                'message' => self::ORDER_CREATE_SUSPENDED,
                'code'    => Response::HTTP_BAD_REQUEST
            ], Response::HTTP_BAD_REQUEST);
        }

        $params = $request->only([
            'amount',
            'buyer',
            'receipt_id',
            'items',
            'buyer_email',
            'city',
            'phone',
            'entry_by',
            'note',
        ]);

        $orderId = $this->orderService->store($params);

        if (! $orderId ) {
            return response()->json([
                'message' => self::ORDER_CREATE_FAILED,
                'code'    => Response::HTTP_BAD_REQUEST
            ], Response::HTTP_BAD_REQUEST);
        }

        Cookie::set($request->get('buyer_email'), true);

        return response()->json([
            'message' => self::ORDER_CREATED_SUCCESSFULLY,
            'code'    => Response::HTTP_CREATED,
            'data'    => Order::find($orderId)
        ], Response::HTTP_CREATED);
    }

}