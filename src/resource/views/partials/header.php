<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>XpeedStudio</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="<?php __e(assets('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php __e(assets('css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php __e(assets('css/style.css')); ?>">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="<?php __e(url('/')); ?>">XpeedStudio</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php __e(url('/home/list')); ?>">All Submission <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <br>