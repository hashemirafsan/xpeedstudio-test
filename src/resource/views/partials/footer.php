    <footer>
        <script src="<?php __e(assets('js/jquery.min.js')); ?>"></script>
        <script src="<?php __e(assets('js/bootstrap.min.js')); ?>"></script>
        <script src="<?php __e(assets('js/popper.min.js')); ?>"></script>
        <script src="<?php __e(assets('js/select2.full.min.js')); ?>"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="<?php __e(assets('js/functions.js')); ?>"></script>
    </footer>
</body>
</html>