<?php view('partials.header'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="buyer">Search Buyer</label>
                <select class="form-control select2_buyer"
                        data-redirect-url="<?php __e(url('/home/list')); ?>"
                        data-search-url="<?php __e(url('/home/search')); ?>" data-key="buyer" id="buyer">
                    <option value="">-</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="buyer">Entry Date</label>
                <input type="text" name="dates" data-redirect-url="<?php __e(url('/home/list')); ?>"
                       data-search-url="<?php __e(url('/home/search')); ?>" data-key="range">
            </div>
        </div>
        <a href="<?php __e(url('/home/list')); ?>" class="mt-4">
            <button class="btn btn-sm btn-warning">Reset</button>
        </a>
    </div>
    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Buyer</th>
                <th scope="col">Receipt ID</th>
                <th scope="col">Buyer Email</th>
                <th scope="col">Buyer IP</th>
                <th scope="col">City</th>
                <th scope="col">Phone</th>
                <th scope="col">Entry ID</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($results as $item) {?>
                    <tr>
                        <th scope="row"><?php __e($item->id); ?></th>
                        <td><?php __e($item->buyer); ?></td>
                        <td><?php __e($item->receipt_id); ?></td>
                        <td><?php __e($item->buyer_email); ?></td>
                        <td><?php __e($item->buyer_ip); ?></td>
                        <td><?php __e($item->city); ?></td>
                        <td><?php __e($item->phone); ?></td>
                        <td><?php __e($item->entry_by); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php view('partials.footer'); ?>