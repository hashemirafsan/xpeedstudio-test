<?php view('partials.header'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form accept-charset="utf-8" data-action="<?php __e(url('/home/store')); ?>" id="xpeedstudio_form">

                        <div class="form-group">
                            <label for="">Receipt ID</label>
                            <input type="text" name="receipt_id" class="form-control" value="">
                            <span class="danger"></span>
                        </div>

                        <div class="form-group">
                            <label for="">Buyer</label>
                            <input type="text" name="buyer" class="form-control" value="">
                            <span class="danger"></span>
                        </div>

                        <div class="form-group">
                            <label for="">Buyer Email</label>
                            <input type="text" name="buyer_email" class="form-control" value="">
                            <span class="danger"></span>
                        </div>

                        <div class="form-group">
                            <label for="">Item</label>
                            <div class="row template_items_wrapper">
                                <div class="col-md-11">
                                    <input type="text" class="form-control add_items_field" value="">
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-sm btn-success add_item" type="button">Add</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Amount</label>
                            <input type="number" name="amount" class="form-control" value="">
                            <span class="danger"></span>
                        </div>

                        <div class="form-group">
                            <label for="">City</label>
                            <input type="text" name="city" class="form-control" value="">
                            <span class="danger"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" name="phone" class="form-control" value="">
                            <span class="danger"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Entry By</label>
                            <input type="text" name="entry_by" class="form-control" value="">
                            <span class="danger"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Note</label>
                            <textarea name="note" cols="30" rows="5" class="form-control"></textarea>
                            <span class="danger"></span>
                        </div>
                        <div>
                            <button class="btn btn-primary" id="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php view('partials.footer'); ?>
