<?php

if (!function_exists('dd')) {
    function dd()
    {
        echo '<pre>';
        array_map(function($x) {var_dump($x);}, func_get_args());
        die;
    }
}

if (! function_exists('base_dir')) {
    /**
     * @param string $path
     * @return string
     */
    function base_dir(string $path = '') {
        return __DIR__ . '/../' . $path;
    }
}

if (! function_exists('resource_dir')) {
    /**
     * @param string $fallback
     * @return string
     */
    function resource_dir(string $fallback) {
        return base_dir('src') . "/resource/views/" . path_fallback($fallback) . '.php';
    }
}

if (! function_exists('path_fallback')) {
    /**
     * @param $fallback
     * @return string|string[]
     */
    function path_fallback($fallback) {
        return str_replace('.', '/', $fallback);
    }
}

if (! function_exists('view')) {
    /**
     * @param string $resource
     * @param array $variables
     */
    function view(string $resource, array $variables = []) {
        extract($variables);
        require resource_dir($resource);
    }
}

if (! function_exists('response')) {
    /**
     * @return \XpeedStudio\Core\Response
     */
    function response() {
        return (new \XpeedStudio\Core\Response());
    }
}

if (! function_exists('request')) {
    /**
     * @return \XpeedStudio\Core\Request
     */
    function request() {
        return (new \XpeedStudio\Core\Request());
    }
}

if (! function_exists('env')) {
    /**
     * @param string $key
     * @param string $default
     * @return mixed|string
     */
    function env(string $key, $default = '') {
        return $_ENV[$key] ?? $default;
    }
}

if (! function_exists('assets')) {
    /**
     * @param string $fallback
     * @return string
     */
    function assets(string $fallback) {
        return url('/assets/' . $fallback);
    }
}

if (! function_exists('__e')) {
    /**
     * @param $param
     */
    function __e($param) {
        echo $param;
    }
}

if (! function_exists('array_get')) {
    /**
     * @param $data
     * @param $accessor
     * @param null $default
     * @return mixed|null
     */
    function array_get($data, $accessor, $default = null ) {
        $accessorArray = explode('.', $accessor);
        $firstKey = array_shift($accessorArray);
        $value = $data[$firstKey];
        foreach($accessorArray as $key) {
            if (isset($value[$key])) {
                return $default;
            }
            $value = $value[$key];
        }
        return $value;
    }
}

if (! function_exists('url')) {
    /**
     * @param string $path
     * @return string
     */
    function url(string $path) {
        return request()->baseUri() . $path;
    }
}

if (! function_exists('redirect')) {
    /**
     * @param $url
     * @param int $statusCode
     */
    function redirect($url, $statusCode = 303)
    {
        header('Location: ' . url($url), true, $statusCode);
        die();
    }
}

if (! function_exists('camel_case')) {
    function camel_case(string $str, $operator = '_') {
        return str_replace($operator, '', ucwords($str, $operator));
    }
}